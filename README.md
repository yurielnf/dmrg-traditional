# DMRG traditional

Traditional DMRG (warm-up only) implemented as simple as possible, just for teaching purposes.

## Usage

1. install the matrix library, in this case [armadillo](http://arma.sourceforge.net/)

2. compile `g++ main.cpp -O3 -std=c++11 -larmadillo`

3. run `./a.out`



