#include <iostream>
#include<armadillo>

using namespace std;
using namespace arma;

/// The block for a tight-binding chain
struct BlockTB          // The block operations are Hamiltonian specific
{
    int m;
    mat Cfirst, Clast, Id, Sg, H; // operator create at the first site, at the last, the identity, sign and H

    /// one site block
    BlockTB()
    {
        m=2;
        Cfirst=Clast=H=zeros<mat>(m,m);
        Id=Sg=eye<mat>(m,m);
        Cfirst(1,0)=Clast(1,0)=1;
        Sg(1,1)=-1;
    }
    /// combine two blocks
    BlockTB(const BlockTB &b1,const BlockTB &b2)
        :m( b1.m*b2.m )
        ,Id(m,m,fill::eye)
    {
        Cfirst=kron(b1.Cfirst,b2.Id) ;
        Clast=kron(b1.Sg,b2.Clast) ;
        Sg=kron(b1.Sg,b2.Sg) ;
        H= kron(b1.H,b2.Id)+kron(b1.Id,b2.H)
          +kron(b1.Clast,b2.Cfirst.t())+kron(b1.Clast.t(),b2.Cfirst);
    }
    void Rotate(const mat &rot)
    {
        m=rot.n_cols;
        Id=eye<mat>(m,m);
        for(mat *op:{&Cfirst,&Clast,&Sg,&H})
        {
            mat &O=*op;
            O=rot.t()*O*rot;
        }
    }
};

template<typename block_t>
struct DMRG                      // DMRG doesn't depend on the Hamiltonian, it needs a block type
{
    int m;
    block_t b1,b2, pto;
    double ener;

    DMRG(int m):m(m){}
    void Iterate()
    {
        b1=block_t(b1,pto);                         // Blocking
        b2=block_t(pto,b2);
        block_t b=block_t(b1,b2);
        vec eval;                                   // Solving
        mat evec;
        eig_sym(eval,evec,b.H);
        ener=eval(0);
        mat psi=reshape(evec.col(0),b2.m,b1.m).t();
        b1.Rotate(FindRot(psi));                    // Decimation
        b2.Rotate(FindRot(psi.t()));
    }
    mat FindRot(const mat& psi)
    {
        mat rho=psi*psi.t();
        vec eval;
        mat evec;
        eig_sym(eval,evec,rho);
        int mm=min(m,(int)eval.size());
        return evec.tail_cols(mm);
    }
};

double ExactEnergyTB(int L, int nPart,bool periodic)
{
    std::vector<double> evals(L);
    for(int k=0;k<L;k++)
    {
        double kf= periodic ? 2*M_PI*k/L: M_PI*(k+1)/(L+1);
        evals[k]=2*cos(kf);
    }
    std::sort(evals.begin(),evals.end());
    double sum=0;
    for(int k=0;k<nPart;k++)
        sum+=evals[k];
    return sum;
}

int main()
{
    cout << "DMRG warm up\n" << endl;
    int len=100, m=20;
    DMRG<BlockTB> sol(m);          // Here another system can be solved specifing other block type

    cout<<"length DMRG_energy exact_energy\n";
    for(int i=2;i<=len/2;i++)
    {
        sol.Iterate();
        cout<<2*i<<" "<<sol.ener<<" "<<ExactEnergyTB(2*i,i,false)<<"\n";
    }
    return 0;
}
